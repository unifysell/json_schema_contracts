defmodule JsonSchemaContractsTest do
  use ExUnit.Case
  doctest JsonSchemaContracts

  test "greets the world" do
    assert JsonSchemaContracts.hello() == :world
  end

  defp load_schema(path) do
    File.read!(path)
    |> Poison.Parser.parse!()
  end

  test "resolve base-types.schema.json" do
    load_schema("schema/base-types.schema.json")
    |> ExJsonSchema.Schema.resolve()
  end

  test "resolve keycloak-user-attribute.schema.json" do
    load_schema("schema/keycloak-user-attribute.schema.json")
    |> ExJsonSchema.Schema.resolve()
  end

  test "resolve banking-connector-job.schema.json" do
    load_schema("schema/banking-connector-job.schema.json")
    |> ExJsonSchema.Schema.resolve()
  end

  test "amazon.valid.test.json is valid" do
    schema = load_schema("schema/keycloak-user-attribute.schema.json")
    payload = load_schema("example/keycloak-user-attribute/amazon.valid.test.json")
    assert ExJsonSchema.Validator.valid?(schema, payload)
  end

  test "amazon.invalid.test.json is invalid" do
    schema = load_schema("schema/keycloak-user-attribute.schema.json")
    payload = load_schema("example/keycloak-user-attribute/amazon.invalid.test.json")
    assert ExJsonSchema.Validator.valid?(schema, payload) == false
  end

  test "banking.valid.test.json is valid" do
    schema = load_schema("schema/keycloak-user-attribute.schema.json")
    payload = load_schema("example/keycloak-user-attribute/banking.valid.test.json")
    assert ExJsonSchema.Validator.valid?(schema, payload)
  end

  test "banking.valid.test.json is valid against keycloak-user-attribute.banking.schema.json" do
    schema = load_schema("schema/keycloak-user-attribute.banking.schema.json")
    payload = load_schema("example/keycloak-user-attribute/banking.valid.test.json")

    assert ExJsonSchema.Validator.valid?(schema, payload)
  end

  test "banking.invalid.test.json is invalid against keycloak-user-attribute.banking.schema.json" do
    schema = load_schema("schema/keycloak-user-attribute.banking.schema.json")
    payload = load_schema("example/keycloak-user-attribute/banking.invalid.test.json")
    assert ExJsonSchema.Validator.valid?(schema, payload) == false
  end

  test "banking-connector-job.valid.test.json is valid against banking-connector-job.schema.json" do
    schema = load_schema("schema/banking-connector-job.schema.json")
    payload = load_schema("example/banking-connector-job/banking-connector-job.valid.test.json")
    assert ExJsonSchema.Validator.valid?(schema, payload)
  end
end
