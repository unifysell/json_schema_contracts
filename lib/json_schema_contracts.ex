defmodule JsonSchemaContracts do
  @moduledoc """
  Documentation for JsonSchemaContracts.
  """

  @doc """
  Hello world.

  ## Examples

      iex> JsonSchemaContracts.hello()
      :world

  """
  def hello do
    :world
  end

  defp load_schema(path) do
    File.read!(path)
    |> Poison.Parser.parse!()
  end

  def test do
    schema = load_schema("schema/banking-connector-job.schema.json")
    payload = load_schema("example/banking-connector-job/banking-connector-job.valid.test.json")
    ExJsonSchema.Validator.validate(schema, payload)
  end
end
